(function ($) {
    $(document).ready(function () {
        $('body').addClass('slowImg');
        
        function iframeFix(){
            $('iframe').wrap('<div class="fix-iframe" />');
            $('.fix-iframe').prepend('<img src="/themes/idweb/img/bg_iframe.gif" />');
        }
        iframeFix();

        $( "#menu-btn" ).click(function() {
            $( this ).parent().find('nav#block-idweb-main-menu ul').slideToggle();
            $( this ).toggleClass('is-active');
        });
    });
})(jQuery);