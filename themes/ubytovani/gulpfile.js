var gulp = require('gulp'); // buildovaci system
var sass = require('gulp-sass'); // sass plugin pro Gulp
var watch = require('gulp-watch'); // hlida změnu v souborech
var sourcemaps = require('gulp-sourcemaps'); // sourcemaps
var uglifycss = require('gulp-uglifycss'); // minify CSS
var criticalCss = require('gulp-critical-css'); // critical CSS tag critical:this
var minifyjs = require('gulp-js-minify'); // minify JS

// Compile Our Sass
gulp.task('sass', function () {
  return gulp.src('./sass/**/*.scss')
    .pipe(sourcemaps.init()) //inicializace sourcemaps
    .pipe(sass.sync().on('error', sass.logError))
      .pipe(criticalCss())
    .pipe(uglifycss({
      "maxLineLen": 80,
      "uglyComments": true
    }))
    .pipe(sourcemaps.write('./')) //cesta kam ulozit sourcemapu
    .pipe(gulp.dest('./css'));
});

gulp.task('minify-js', function(){
    gulp.src('./js/global.js')
        .pipe(minifyjs())
        .pipe(gulp.dest('./js/global.min.js'));
});


gulp.task('watch', function() {
	gulp.watch('./sass/**/*', ['sass']);
});

// Default task
gulp.task('default', ['sass', 'watch','minify-js']);